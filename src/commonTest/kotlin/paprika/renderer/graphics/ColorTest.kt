package paprika.renderer.graphics

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class ColorTest {
    @Test
    fun `Convert 0xFFFFFFFF to color`() {
        colorOf(0xFFFFFFFF).apply {
            assertEquals(red, 1f)
            assertEquals(blue, 1f)
            assertEquals(green, 1f)
            assertEquals(alpha, 1f)
        }
    }

    @Test
    fun `Convert 0xFFFFFF no alpha to color`() {
        colorOf(0xFFFFFF, false).apply {
            assertEquals(red, 1f)
            assertEquals(blue, 1f)
            assertEquals(green, 1f)
            assertEquals(alpha, 1f)
        }
    }

    @Test
    fun `Convert 0xE2F4D2 no alpha to color`() {
        colorOf(0xE2F4D2, false).apply {
            assertEquals(redChanel, 226)
            assertEquals(greenChanel, 244)
            assertEquals(blueChanel, 210)
            assertEquals(alpha, 1f)
        }
    }

    @Test
    fun `Convert 0 alpha to color`() {
        colorOf(0).apply {
            assertEquals(red, 0f)
            assertEquals(blue, 0f)
            assertEquals(green, 0f)
            assertEquals(alpha, 0f)
        }
    }

    @Test
    fun `Convert -1 should fail`() {
        assertFailsWith<UnsuportedValueException> { colorOf(-1, false) }
    }

    @Test
    fun `Convert 0xFFFFFFF should fail no alpha`() {
        assertFailsWith<UnsuportedValueException> { colorOf(0xFFFFFFF, false) }
    }

    @Test
    fun `Convert 0xFFFFFFFFF should fail alpha`() {
        assertFailsWith<UnsuportedValueException> { colorOf(0xFFFFFFFFF, true) }
    }
}
