package ca.jsbr.paprika.engine.material

import paprika.koml.geom.MutableMatrix4f
import paprika.render.pglObject.PGLBuffer
import paprika.render.pglObject.PGLProgram
import paprika.renderer.PGL
import paprika.renderer.shader.Shader

class DefaultMaterial(val shader: Shader): Material {

    companion object {
        const val POSITION_ATTRIBUTE = "a_position"
        const val TEXCOORD_ATTRIBUTE = "a_texcoord"
        const val MATRIX_UNIFORM = "u_matrix"
        const val COLOR_ATTRIBUTE = "a_color"
        const val TEXTURE = "u_texture"
    }

    override val shaderProgram: PGLProgram = shader.create()

    override fun setCoordinates(positionBuffer: PGLBuffer) = shader.setAttribute(DefaultSpriteMaterial.POSITION_ATTRIBUTE, positionBuffer, 3, PGL.FLOAT)
    override fun setTextureCoordinates(texcoordBuffer: PGLBuffer) = shader.setAttribute(DefaultSpriteMaterial.TEXCOORD_ATTRIBUTE, texcoordBuffer, 2, PGL.FLOAT)
    override fun setMatrix(matrix: MutableMatrix4f) = shader.getConstantProperty(DefaultSpriteMaterial.MATRIX_UNIFORM).setMatrix4f(matrix)
    override fun setColor(colorBuffer: PGLBuffer) = shader.setAttribute(DefaultSpriteMaterial.COLOR_ATTRIBUTE, colorBuffer, 4, PGL.FLOAT)
}