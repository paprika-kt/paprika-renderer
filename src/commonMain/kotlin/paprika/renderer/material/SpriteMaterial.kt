package ca.jsbr.paprika.engine.material

import paprika.renderer.graphics.texture.Texture

interface SpriteMaterial: Material {
    fun setTexture(texture: Texture)
}