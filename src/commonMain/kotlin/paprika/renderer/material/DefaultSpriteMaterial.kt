package ca.jsbr.paprika.engine.material

import paprika.renderer.graphics.texture.Texture
import paprika.koml.geom.MutableMatrix4f
import paprika.renderer.shader.Shader
import paprika.renderer.PGL
import paprika.render.pglObject.*
import paprika.render.pglObject.*

class DefaultSpriteMaterial(val shader: Shader) : SpriteMaterial {

    override val shaderProgram: PGLProgram = shader.getOrCreate()

    companion object {
        const val POSITION_ATTRIBUTE = "a_position"
        const val TEXCOORD_ATTRIBUTE = "a_texcoord"
        const val MATRIX_UNIFORM = "u_matrix"
        const val COLOR_ATTRIBUTE = "a_color"
        const val TEXTURE = "u_texture"
    }

    override fun setCoordinates(positionBuffer: PGLBuffer) = shader.setAttribute(POSITION_ATTRIBUTE, positionBuffer, 2, PGL.FLOAT)
    override fun setTextureCoordinates(texcoordBuffer: PGLBuffer) = shader.setAttribute(TEXCOORD_ATTRIBUTE, texcoordBuffer, 2, PGL.FLOAT)
    override fun setMatrix(matrix: MutableMatrix4f) = shader.getConstantProperty(MATRIX_UNIFORM).setMatrix4f(matrix)
    override fun setTexture(texture: Texture) = shader.getConstantProperty(TEXTURE).setTexture(texture)
    override fun setColor(colorBuffer: PGLBuffer) = shader.setAttribute(COLOR_ATTRIBUTE, colorBuffer, 4, PGL.FLOAT)
}
