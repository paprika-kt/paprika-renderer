package ca.jsbr.paprika.engine.material

import paprika.koml.geom.MutableMatrix4f
import paprika.render.pglObject.PGLBuffer
import paprika.render.pglObject.PGLProgram

interface Material {
    val shaderProgram: PGLProgram
    fun setCoordinates(positionBuffer: PGLBuffer)
    fun setTextureCoordinates(texcoordBuffer: PGLBuffer)
    fun setColor(colorBuffer: PGLBuffer)
    fun setMatrix(matrix: MutableMatrix4f)
}