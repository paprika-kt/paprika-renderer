package paprika.renderer.graphics

import paprika.koml.geom.*
import paprika.render.RenderingContext
import paprika.renderer.graphics.sprite.Quad

interface Camera {
    val perspective: MutableMatrix4f
    val viewport: Rectangle
    fun update(width: Int, height: Int)
    fun unProject(vector3f: MutableVector3f, canvasHeight: Int): MutableVector3f
    fun unProject(vector3f: MutableVector3f): MutableVector3f
}

fun unProject(screenCoords: MutableVector3f, canvasHeight: Int, viewport: Rectangle, perspectiveInvert: MutableMatrix4f): MutableVector3f {
    val (viewportX, viewportY, viewportWidth, viewportHeight) = viewport
    var (x, y) = screenCoords
    x -= viewportX
    y = canvasHeight - y - 1
    y -= viewportY
    screenCoords.set(
            (2 * x) / viewportWidth - 1,
            (2 * y) / viewportHeight - 1,
            2 * screenCoords.z - 1
    )
    screenCoords.prj(perspectiveInvert)
    return screenCoords
}

fun Quad.flipVertical(): Quad {
    return Quad(texture, x, y, width, height, u2, v, u, v2, colors.copyOf())
}

enum class ScreenFill(val update: ScreenFillFun) {

    Cover(Camera2D::cover),
    ContainWidth(Camera2D::containWidth),
    ContainHeight(Camera2D::containHeight),
    Contain(Camera2D::contain),
    Fit(Camera2D::fit),
    Resize(Camera2D::resize),
    FitHeight(Camera2D::fitHeight),

}

fun Camera2D.resize(canvasWidth: Int, canvasHeight: Int) {
    perspective.set(MutableMatrix4f.orthographic(0f, canvasWidth.toFloat(), canvasHeight.toFloat(), 0f, -1f, 1f))
    perspective.orthographic(0f, canvasWidth.toFloat(), canvasHeight.toFloat(), 0f, -1f, 1f)
    viewport.set(0, 0, canvasWidth, canvasHeight)
}

typealias ScreenFillFun = Camera2D.(canvasWidth: Int, canvasHeight: Int) -> Unit

fun Camera2D.contain(canvasWidth: Int, canvasHeight: Int) {
    val ratio = designWidth / designHeight.toFloat()
    val r = canvasWidth / canvasHeight.toFloat()
    if (r > ratio) containHeight(canvasWidth, canvasHeight)
    else containWidth(canvasWidth, canvasHeight)
}

fun Camera2D.containHeight(canvasWidth: Int, canvasHeight: Int) {
    val ratio = designWidth / designHeight.toFloat()
    val w = (canvasHeight * ratio).toInt()
    viewport.set((canvasWidth - w) / 2, 0, w, canvasHeight)
}

fun Camera2D.containWidth(canvasWidth: Int, canvasHeight: Int) {
    val ratio = designWidth / designHeight.toFloat()
    val h = (canvasWidth * (1 / ratio)).toInt()
    viewport.set(0, (canvasHeight - h) / 2, canvasWidth, h)
}

fun Camera2D.cover(canvasWidth: Int, canvasHeight: Int) {
    viewport.set(0, 0, canvasWidth, canvasHeight)
}

fun Camera2D.fit(canvasWidth: Int, canvasHeight: Int) {
    val ratio = designWidth / designHeight.toFloat()
    val r = canvasWidth / canvasHeight.toFloat()
    if (r < ratio) containHeight(canvasWidth, canvasHeight)
    else containWidth(canvasWidth, canvasHeight)
}

fun Camera2D.fitHeight(canvasWidth: Int, canvasHeight: Int) {

    val ratio = designWidth / designHeight.toFloat()
    val w = (canvasHeight * ratio)
    val ratioPerspective = canvasWidth / w

    perspective.set(MutableMatrix4f.orthographic(0f, designWidth * (ratioPerspective), designHeight.toFloat(), 0f, -1f, 1f))

    viewport.set(0, 0, canvasWidth, canvasHeight)
}

fun MutableMatrix4f.orthographic(left: Float, right: Float, bottom: Float, top: Float, near: Float, far: Float): MutableMatrix4f {
    val rl = (right - left)
    val tb = (top - bottom)
    val fn = (far - near)

    return set(
            2f / rl, 0f, 0f, 0f,
            0f, 2f / tb, 0f, 0f,
            0f, 0f, -2f / fn, 0f,
            -(left + right) / rl, -(top + bottom) / tb, -(far + near) / fn, 1f
    )
}


class Camera2D(val renderingContext: RenderingContext, val designWidth: Int, val designHeight: Int, val fill: ScreenFillFun) : Camera {
    private var canvasHeight: Int = 0
    override val perspective: MutableMatrix4f = MutableMatrix4f.orthographic(0f, designWidth.toFloat(), designHeight.toFloat(), 0f, -1f, 1f)
    override val viewport = MutableRectangle()


    private val perspectiveInverse = MutableMatrix4f()

    constructor(renderingContext: RenderingContext, width: Int, height: Int, fill: ScreenFill = ScreenFill.Contain) : this(renderingContext, width, height, fill.update)

    init {
    }

    override fun unProject(vector3f: MutableVector3f, canvasHeight: Int): MutableVector3f {
        perspectiveInverse.set(perspective).inverse()
        return unProject(vector3f, canvasHeight, viewport, perspectiveInverse)
    }

    override fun unProject(vector3f: MutableVector3f): MutableVector3f = unProject(vector3f, canvasHeight)

    override fun update(width: Int, height: Int) {
        canvasHeight = height
        fill(this, width, height)
        renderingContext.viewport(viewport.x, viewport.y, viewport.width, viewport.height)
    }
}