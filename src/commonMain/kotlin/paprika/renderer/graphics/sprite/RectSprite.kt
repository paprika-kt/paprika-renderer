package paprika.renderer.graphics.sprite


interface RectSprite : Sprite {
    override fun clone(): RectSprite
    var x: Float
    var y: Float
    var width: Float
    var height: Float
}
