package paprika.renderer.graphics.sprite

import paprika.renderer.graphics.texture.Texture

class PolySprite(
    override var texture: Texture,
    override var vertices: FloatArray,
    override var textCoord: FloatArray,
    override var indices: ShortArray,
    override var colors: FloatArray) : Sprite {

    override fun clone(): Sprite {
        return PolySprite(texture, vertices, textCoord, indices, colors)
    }
}