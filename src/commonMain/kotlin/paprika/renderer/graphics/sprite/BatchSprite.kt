package paprika.renderer.graphics.sprite

import paprika.renderer.graphics.texture.Texture



class BatchSpriteBuilder(val batchSize: Int = 100) {

    private var texture: Texture? = null
    private var verticesCount: Int = 0
    private var indicesCount: Int = 0
    private val vertices = ArrayList<Float>()
    private val texCoord = ArrayList<Float>()
    private val colors = ArrayList<Float>()
    private val indices = ArrayList<Short>()

    fun add(sprite: Sprite): Boolean {
        return add(sprite.texture, sprite.vertices, sprite.textCoord, sprite.indices, sprite.colors)
    }

    fun add(texture: Texture, vertices: FloatArray, textCoord: FloatArray, indices: ShortArray, colors: FloatArray): Boolean {
        val len = vertices.size / 2
        if (batchSize < verticesCount + len * 2) {
            return false
        }

        if (this.texture !== texture && this.texture != null) {
            return false
        }


        this.texture = texture
        var count = 0

        while (count < len) {

            var index = count * 2
            var start = verticesCount * 2
//vertices
            this.vertices.add(vertices[index])
            this.vertices.add(vertices[index + 1])
//texture coordinate
            this.texCoord.add(textCoord[index])
            this.texCoord.add(textCoord[index + 1])
//colors
            index = count * 4
            start = verticesCount * 4
            this.colors.add(colors[index])
            this.colors.add(colors[index + 1])
            this.colors.add(colors[index + 2])
            this.colors.add(colors[index + 3])
            count++
        }

        for (it in indices) {
            this.indices.add((it + verticesCount).toShort())
        }

        verticesCount += len
        return true
    }

    fun build(): Sprite =
        PolySprite(texture!!, vertices.toFloatArray(), texCoord.toFloatArray(), indices.toShortArray(), colors.toFloatArray())
}

class MultiSpriteBuilder(val batchSize: Int = 100) {
    val textures = HashMap<Texture, ArrayList<BatchSpriteBuilder>>()

    fun add(sprite: Sprite): Boolean {
        return add(sprite.texture, sprite.vertices, sprite.textCoord, sprite.indices, sprite.colors)
    }

    fun add(texture: Texture, vertices: FloatArray, textCoord: FloatArray, indices: ShortArray, colors: FloatArray): Boolean {
        val batch = textures[texture]?.last() ?: createBatch(texture)
        val result = batch.add(texture, vertices, textCoord, indices, colors)
        if (!result)
            createBatch(texture).add(texture, vertices, textCoord, indices, colors)
        return true
    }

    private fun createBatch(texture: Texture): BatchSpriteBuilder {
        val batchSpriteBuilder = BatchSpriteBuilder()
        val list = textures.getOrPut(texture) { ArrayList() }
        list.add(batchSpriteBuilder)
        return batchSpriteBuilder
    }

    fun build() = textures.values.flatten().map { it.build() }
}