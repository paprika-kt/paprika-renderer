package paprika.renderer.graphics.sprite

import paprika.renderer.graphics.texture.Texture

interface Sprite {
    val texture: Texture
    val vertices: FloatArray
    val textCoord: FloatArray
    val colors: FloatArray
    val indices: ShortArray

    fun clone(): Sprite
}

