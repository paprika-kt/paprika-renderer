package paprika.renderer.graphics.sprite

import paprika.renderer.graphics.Color
import paprika.renderer.graphics.ReadOnlyColor
import paprika.koml.geom.Matrix3f
import paprika.koml.geom.RectangleF
import paprika.koml.geom.Sizef
import paprika.koml.geom.Matrix4f
import paprika.koml.geom.transform2d

fun Sprite.setColor(r: Float, g: Float, b: Float, a: Float = 1f): Sprite {
    for (ind in this.colors.indices step 4) {
        this.colors[ind] = r
        this.colors[ind + 1] = g
        this.colors[ind + 2] = b
        this.colors[ind + 3] = a
    }
    return this
}

fun Sprite.setColor(r: Color): Sprite {
    for (ind in this.colors.indices step 4) {
        this.colors[ind] = r.red
        this.colors[ind + 1] = r.green
        this.colors[ind + 2] = r.blue
        this.colors[ind + 3] = r.alpha
    }
    return this
}

var Sprite.color: Color
    get() = ReadOnlyColor(this.colors[0], this.colors[1], this.colors[2], this.colors[3])
    set(color) {
        setColor(color.red, color.green, color.blue, color.alpha)
    }


fun Sprite.transform(matrix4f: Matrix4f, source: Sprite = this) {
    matrix4f.transform2d(source.vertices, this.vertices)
}

fun Sprite.transform(source: FloatArray, matrix4f: Matrix4f) {
    matrix4f.transform2d(source, this.vertices)
}

fun Sprite.transform(source: FloatArray, matrix3f: Matrix3f) {
    matrix3f.transform2d(source, this.vertices)
}

fun Sprite.transform(matrix3f: Matrix3f, source: Sprite = this) {
    matrix3f.transform2d(source.vertices, this.vertices)
}


val Sprite.size: Sizef
    get() {
        val x = vertices.filterIndexed { index, fl -> index % 2 == 0 }
        val y = vertices.filterIndexed { index, fl -> index % 2 == 1 }
        return Sizef(x.max()!! - x.min()!!, y.max()!! - y.min()!!)
    }

val Sprite.rectangle: RectangleF
    get() {
        val xVertices = vertices.filterIndexed { index, fl -> index % 2 == 0 }
        val yVertices = vertices.filterIndexed { index, fl -> index % 2 == 1 }
        val x = xVertices.min()!!
        val y = yVertices.min()!!
        return RectangleF(x, y, xVertices.max()!! - x, yVertices.max()!! - y)
    }

val Sprite.verticesWidth: Float
    get() {
        val x = vertices.filterIndexed { index, fl -> index % 2 == 0 }
        return x.max()!! - x.min()!!
    }


val Sprite.verticesHeight: Float
    get() {
        val y = vertices.filterIndexed { index, fl -> index % 2 == 1 }
        return y.max()!! - y.min()!!
    }

fun Quad.cropRect(rect: RectangleF): Quad {
    val nu = u + rect.x / texture.width
    val nv = v + rect.y / texture.height
    return Quad(texture, x, y, rect.width, rect.height, nu, nv, nu + rect.width / texture.width, nv + rect.height / texture.height)
}


fun merge(vararg sprites: Sprite): Sprite {
    val vertices = ArrayList<Float>()
    val textCoord = ArrayList<Float>()
    val colors = ArrayList<Float>()
    val indices = ArrayList<Short>()

    sprites.forEach { sprite ->
        val size = vertices.size/2
        sprite.vertices.forEach { vertices.add(it) }
        sprite.textCoord.forEach { textCoord.add(it) }
        sprite.colors.forEach { colors.add(it) }
        sprite.indices.forEach { indices.add((it + size).toShort()) }
    }

    return PolySprite(sprites[0].texture,
        vertices.toFloatArray(),
        textCoord.toFloatArray(),
        indices.toShortArray(),
        colors.toFloatArray())
}

fun Quad.cropBox(x: Float = 0f, y: Float = 0f, right: Float = 0f, down: Float = 0f): Quad =
    Quad(texture, 0f, 0f, right - x, down - y, u + x / texture.width, v + y / texture.height, u + right / texture.width, v + down / texture.height)


@Suppress("unused")
fun Quad.cropRect(x: Float = 0f, y: Float = 0f, width: Float = 0f, height: Float = 0f): Quad {
    val nu = u + x / texture.width
    val nv = v + y / texture.height
    return Quad(texture, 0f, 0f, width, height, nu, nv, nu + width / texture.width, nv + height / texture.height)
}
@Suppress("unused")
fun Quad.crop(top: Float = 0f, left: Float = 0f, bottom: Float = 0f, right: Float = 0f): Quad =
    Quad(texture, x, y, width - left - right, height - top - bottom, u + left / texture.width, v + top / texture.height, u2 - right / texture.width, v2 - bottom / texture.height)

