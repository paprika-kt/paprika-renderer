package paprika.renderer.graphics.sprite

import paprika.renderer.graphics.texture.Texture
import paprika.koml.geom.RectangleF

class Quad(
        override var texture: Texture, x: Float = 0f, y: Float = 0f,
        width: Float = texture.width.toFloat(), height: Float = texture.height.toFloat(),
        val u: Float = 0f, val v: Float = 0f, val u2: Float = 1f, val v2: Float = 1f,
        override val colors: FloatArray = FloatArray(16) { 1f }
) : RectSprite {

    private var _x: Float = x
    override var x: Float
        get() = _x
        set(value) {
            _x = value
            vertices = createVertices(_x, _y, width, height)
        }

    private var _y: Float = y
    override var y: Float
        get() = _y
        set(value) {
            _y = value
            vertices = createVertices(_x, _y, _width, _height)
        }

    private var _width: Float = width
    override var width: Float
        get() = _width
        set(value) {
            _width = value
            vertices = createVertices(_x, _y, _width, _height)
        }

    private var _height: Float = height
    override var height: Float
        get() = _height
        set(value) {
            _height = value
            vertices = createVertices(_x, _y, _width, _height)
        }

    private val _rectangle = RectangleF()
    val rectangle: RectangleF
        get() = _rectangle.set(x, y, width, height)


    companion object {
        fun fromRegion(texture: Texture, srcX: Int, srcY: Int, srcWidth: Int, srcHeight: Int, offsetX: Float = 0f, offsetY: Float = 0f) =
            Quad(texture, offsetX, offsetY, srcWidth.toFloat(), srcHeight.toFloat(), srcX.toFloat() / texture.width, srcY.toFloat() / texture.height, (srcWidth.toFloat() + srcX) / texture.width.toFloat(), (srcHeight.toFloat() + srcY) / texture.height.toFloat())
    }

    override var vertices: FloatArray = createVertices(x, y, width, height)
        private set

    override val textCoord: FloatArray = floatArrayOf(
        u, v,
        u, v2,
        u2, v2,
        u2, v
    )
    override val indices: ShortArray = shortArrayOf(3, 2, 1, 3, 1, 0)

    fun set(x: Float = 0f, y: Float = 0f,width: Float = this.width, height: Float = this.height){
        _x = x
        _y = y
        _width = width
        _height = height
        vertices = createVertices(_x, _y, width, height)
    }

    fun translate(x: Float = 0f, y: Float = 0f) {
        _x += x
        _y += y
        vertices = createVertices(_x, _y, width, height)
    }

    fun resize(width: Float = this.width, height: Float = this.height) {
        _width = width
        _height = height
        vertices = createVertices(_x, _y, width, height)
    }

    override fun clone(): Quad {
        return Quad(texture, x, y, width, height, u, v, u2, v2, colors.copyOf())
    }

    private fun createVertices(x: Float, y: Float, width: Float, height: Float): FloatArray {
        return floatArrayOf(
            x, y,
            x, y + height,
            x + width, y + height,
            x + width, y)
    }
}