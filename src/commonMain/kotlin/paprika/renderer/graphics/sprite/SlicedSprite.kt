package paprika.renderer.graphics.sprite

import paprika.renderer.graphics.texture.Texture
import paprika.koml.geom.*

/**
 * Todo review, all primary constructor should have the sprite interface property as paramter
 */
class SlicedSprite(
    override val texture: Texture,
    private val top: Int,
    private val right: Int,
    private val bottom: Int,
    private val left: Int,
    val srcX: Int = 0,
    val srcY: Int = 0,
    var srcWidth: Int = texture.width,
    var srcHeight: Int = texture.height,
    offsetX: Float = 0f, offsetY: Float = 0f, width: Float = srcWidth.toFloat(), height: Float = srcHeight.toFloat()
) : RectSprite {



    private var _x: Float = offsetX
    override var x: Float
        get() = _x
        set(value) {
            _x = value
            vertices = createVertices(_x, _y, width, height)
        }

    private var _y: Float = offsetY
    override var y: Float
        get() = _y
        set(value) {
            _y = value
            vertices = createVertices(_x, _y, _width, _height)
        }

    private var _width: Float = width
    override var width: Float
        get() = _width
        set(value) {
            _width = value
            vertices = createVertices(_x, _y, _width, _height)
        }

    private var _height: Float = height
    override var height: Float
        get() = _height
        set(value) {
            _height = value
            vertices = createVertices(_x, _y, _width, _height)
        }


    override var vertices: FloatArray = createVertices(_x, _y, width, height)
    override var textCoord: FloatArray = createTextCoord()
    override val indices: ShortArray = shortArrayOf(
        0, 4, 5, 0, 1, 5,
        1, 5, 6, 1, 2, 6,
        2, 6, 7, 2, 3, 7,

        4, 8, 9, 4, 5, 9,
        5, 9, 10, 5, 6, 10,
        6, 10, 11, 6, 7, 11,

        8, 12, 13, 8, 9, 13,
        9, 13, 14, 9, 10, 14,
        10, 14, 15, 10, 11, 15
    )
    override val colors: FloatArray = floatArrayOf(
        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,

        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,

        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,

        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f,
        1f, 1f, 1f, 1f
    )

    fun transform(matrix4f: MutableMatrix4f) {

        val matrixScale = MutableMatrix4f().scale(matrix4f.scaleX, matrix4f.scaleY, matrix4f.scaleZ)
        val matrixNoScale = matrix4f.clone().scale(1 / matrix4f.scaleX, 1 / matrix4f.scaleY, 1 / matrix4f.scaleZ)
        var v = matrixScale.transform2d(_x, _y)
        _x = v.x
        _y = v.y
        v = matrixScale.transform2d(width, height)
        _width = v.x
        _height = v.y
        vertices = createVertices(_x, _y, width, height)
        matrixNoScale.transform2d(vertices)
        _x = vertices[0]
        _y = vertices[1]
        _width = vertices[vertices.size - 2] - _x
        _height = vertices.last() - _y

    }

    fun translate(x: Float=0f, y: Float =0f) {
        _x += x
        _y += y
        vertices = createVertices(_x, _y, width, height)
    }

    fun transform(x: Float = _x, y: Float = _y, width: Float = this.width, height: Float = this.height) {
        _x = x
        _y = y
        _width = width
        _height = height
        vertices = createVertices(_x, _y, width, height)
    }

    fun crop(width: Float = this.width, height: Float = this.height) {
        this.srcWidth = ((width / this.width) * this.srcWidth).toInt()
        this.srcHeight = ((height / this.height) * this.srcHeight).toInt()
        textCoord = createTextCoord()
        this.width = width
        this.height = height
        vertices = createVertices(_x, _y, width, height)

    }

    fun resize(width: Float = this.width, height: Float = this.height) {
        _width = width
        _height = height
        vertices = createVertices(_x, _y, width, height)
    }

    override fun clone(): SlicedSprite {
        val s = SlicedSprite(texture, top, right, bottom, left, srcX, srcY, srcWidth, srcHeight, x, y, width, height)
        colors.forEachIndexed{ i: Int, fl: Float -> s.colors[i] = fl }
        return s
    }

    private fun createTextCoord(): FloatArray {
        val w = texture.width.toFloat()
        val h = texture.height.toFloat()

        val u1 = srcX / w
        val u2 = (srcX + left) / w
        val u3 = (srcX + srcWidth - right) / w
        val u4 = (srcX + srcWidth) / w

        val v1 = srcY / h
        val v2 = (srcY + top) / h
        val v3 = (srcY + srcHeight - bottom) / h
        val v4 = (srcY + srcHeight) / h

        return floatArrayOf(
            u1, v1, u2, v1, u3, v1, u4, v1,
            u1, v2, u2, v2, u3, v2, u4, v2,
            u1, v3, u2, v3, u3, v3, u4, v3,
            u1, v4, u2, v4, u3, v4, u4, v4
        )

    }

    private fun createVertices(x: Float, y: Float, width: Float, height: Float): FloatArray {
        val line2 = y + top
        val line3 = y + height - bottom
        val line4 = y + height
        val col2 = x + left
        val col3 = x + width - right
        val col4 = x + width

        return floatArrayOf(
            //Line1
            x, y,
            col2, y,
            col3, y,
            col4, y,
            //Line2
            x, line2,
            col2, line2,
            col3, line2,
            col4, line2,
            //Line3
            x, line3,
            col2, line3,
            col3, line3,
            col4, line3,
            //Line4
            x, line4,
            col2, line4,
            col3, line4,
            col4, line4
        )
    }
}

fun Quad.quadToSlicedSprite(top: Int, right: Int, bottom: Int, left: Int): SlicedSprite {
    val x = (u * texture.width).toInt()
    val y = (v * texture.height).toInt()
    return SlicedSprite(texture, top, right, bottom, left, x, y, (u2 * texture.width).toInt() - x, (v2 * texture.height).toInt() - y)
}

fun Sprite.toSlicedSprite(top: Int, right: Int, bottom: Int, left: Int): SlicedSprite = when (this) {
    is Quad -> quadToSlicedSprite(top, right, bottom, left)
    is SlicedSprite -> SlicedSprite(texture, top, right, bottom, left, srcX, srcY, srcWidth, srcHeight, x, y)
    else -> TODO("Not supported")
}