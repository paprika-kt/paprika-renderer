package paprika.renderer.graphics.text

import paprika.koml.geom.*
import paprika.koml.geom.Matrix3f
import paprika.renderer.Renderer2D
import paprika.renderer.graphics.Color
import paprika.renderer.graphics.sprite.Quad
import paprika.renderer.graphics.sprite.Sprite
import paprika.renderer.graphics.sprite.transform
import paprika.renderer.graphics.text.font.*
import paprika.renderer.graphics.texture.Texture


fun Map<Char, Quad>.toFont(): Font {
    val map = this + this.filterKeys { !"123456789".contains(it) }.mapKeys { it.toString().toLowerCase().first() }
    val tallest: Quad = values.maxBy { it.height }!!
    return BasicFont(
        tallest.height * 1.2f,
        tallest.height,
        true,
        -1,
        this[' ']?.width ?: tallest.width * 0.5f,
        0f,
        map.mapValues { SpriteGlyph(it.key, it.value) }
    )
}

fun FNTParser.toFont(textures: List<Texture>) =
    BasicFont(
        lineHeight,
        size,
        true,
        -1,
        spaceWidth,
        ascent,
        glyphs
            .filter { it != null }
            .flatMap { it!!.asIterable() }
            .filter { it != null }
            .map { glyph ->
                val texture = textures[glyph!!.page]
                val w = texture.width.toFloat()
                val h = texture.height.toFloat()
                val sprite = Quad(
                    texture,
                    glyph.xoffset.toFloat(), glyph.yoffset + ascent,
                    glyph.width.toFloat(), glyph.height.toFloat(),
                    glyph.srcX / w, glyph.srcY / h, (glyph.srcX + glyph.width) / w, (glyph.srcY + glyph.height) / h
                )
                glyph.id.toChar() to BasicGlyph(
                    glyph.id.toChar(),
                    sprite,
                    glyph.xoffset.toFloat(),
                    glyph.kerning.mapValues { it.value.toFloat() })
            }.toMap()
    )

fun FNTParser.toFont(textures: Array<Texture>) =
    BasicFont(
        lineHeight,
        size,
        italic,
        if (bold) 700 else 400,
        spaceWidth,
        ascent,
        glyphs
            .filter { it != null }
            .flatMap { it!!.asIterable() }
            .filter { it != null }
            .map { glyph ->
                val texture = textures[glyph!!.page]
                val w = texture.width.toFloat()
                val h = texture.height.toFloat()
                val sprite = Quad(
                    texture,
                    glyph.xoffset.toFloat(), glyph.yoffset + ascent,
                    glyph.width.toFloat(), glyph.height.toFloat(),
                    glyph.srcX / w, glyph.srcY / h, (glyph.srcX + glyph.width) / w, (glyph.srcY + glyph.height) / h
                )
                glyph.id.toChar() to BasicGlyph(
                    glyph.id.toChar(),
                    sprite,
                    glyph.xoffset.toFloat(),
                    glyph.kerning.mapValues { it.value.toFloat() })
            }.toMap()
    )

val fontArray = FloatArray(8)
val drawFontMatrix = MutableMatrix3f()
fun Renderer2D.draw(font: Font, text: String, matrix3f: Matrix3f) {
    val m2 = drawFontMatrix.set(matrix3f)
    var line = 0

    var prev: Char? = null
    for (it in text) {
        if (it == ' ') {
            m2.translate(font.spaceWidth, 0f)
            continue
        }
        if (it == '\n') {
            line++
            drawFontMatrix.set(matrix3f)
            m2.translate(0f, font.lineHeight * line)
            continue
        }

        val glyph = font[it] ?: error("character $it not found")
        val sprite = glyph.sprite
        draw(
            sprite.texture,
            m2.transform2d(sprite.vertices, fontArray),
            sprite.textCoord,
            sprite.indices,
            sprite.colors
        )

        val kerning = if (prev != null) glyph.getKerning(prev) else 0f
        m2.translate(sprite.width + glyph.xOffset - kerning, 0f)
        prev = it
    }
    return
}

fun Font.write(text: String, matrix4f: MutableMatrix4f = MutableMatrix4f()): List<Sprite> {
    val m2 = matrix4f.clone()
    var prev: Char? = null
    val result = ArrayList<Sprite>()
    for (it in text) {
        if (it == ' ') {
            m2.translate(this.spaceWidth, 0f, 0f)
            continue
        }

        val glyph = this[it] ?: TODO("character $it not found")
        val sprite = glyph.sprite.clone()
        sprite.transform(m2)
        result.add(sprite)

        val kerning = if (prev != null) glyph.getKerning(prev) else 0f
        m2.translate(sprite.width + glyph.xOffset - kerning, 0f, 0f)
        prev = it
    }
    return result
}

fun Font.textWidth(text: String): Float {
    var prev: Char? = null
    var result: Float = 0f
    for (it in text) {
        if (it == ' ') {
            result += this.spaceWidth
            continue
        }

        val glyph = this[it] ?: TODO("character $it not found")
        val sprite = glyph.sprite.clone()

        val kerning = if (prev != null) glyph.getKerning(prev) else 0f
        result += sprite.width + glyph.xOffset - kerning
        prev = it
    }
    return result
}

fun Font.transform(size: Float = this.size, color: Color? = null, lineHeight: Float? = null): Font {
    val resize = size / this.size
    return BasicFont(
        lineHeight ?: this.lineHeight * resize,
        size,
        italic, weight,
        spaceWidth * resize,
        ascent,
        glyphs.mapValues {
            TransformedGlyph(
                resize,
                color,
                it.value
            )
        }
    )
}
