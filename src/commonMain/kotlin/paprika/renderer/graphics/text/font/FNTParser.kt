package paprika.renderer.graphics.text.font

import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.min


private val LOG2_PAGE_SIZE = 9
private val PAGE_SIZE = 1 shl LOG2_PAGE_SIZE
private val PAGES = 0x10000 / PAGE_SIZE


/** Represents a single character in a font page.  */
class FNTGlyph2 {
    var id: Int = 0
    var srcX: Int = 0
    var srcY: Int = 0
    var width: Int = 0
    var height: Int = 0
    var u: Float = 0f
    var v: Float = 0f
    var u2: Float = 0f
    var v2: Float = 0f
    var xoffset: Int = 0
    var yoffset: Int = 0
    var xadvance: Int = 0
    var kerning: HashMap<Char, Int> = HashMap()
    var fixedWidth: Boolean = false

    /** The index to the texture page that holds this glyph.  */
    var page = 0

    fun getKerning(ch: Char): Int = kerning[ch] ?: 0

    fun setKerning(ch: Int, value: Int) {
        kerning[ch.toChar()] = value
    }

    override fun toString(): String =
        id.toChar().toString()
}



@Suppress("unused", "MemberVisibilityCanPrivate")
class FNTParser() {

    val MAX_CHAR = '\uFFFF'
    var lineHeight: Float = 0f    // The distance from one line of text to the next. To set this value, use [.setLineHeight].
    var spaceWidth: Float = 0.toFloat()    //The width of the space character.
    var size: Float = 0f // Font-size  */
    var pages: Array<String> = arrayOf() //An array of the image paths, for multiple texture pages.

    var flipped: Boolean = false
    var padTop: Float = 0f
    var padRight: Float = 0f
    var padBottom: Float = 0f
    var padLeft: Float = 0f
    var face: String = ""

    /** The distance from the top of most uppercase characters to the baseline. Since the drawing position is the cap height of
     * the first line, the cap height can be used to get the location of the baseline.  */
    var capHeight = 1f
    /** The distance from the cap height to the top of the tallest glyph.  */
    var ascent: Float = 0f
    /** The distance from the bottom of the glyph that extends the lowest to the baseline. This number is negative.  */
    var descent: Float = 0f
    /** The distance to move down when \n is encountered.  */
    var down: Float = 0f
    /** Multiplier for the line height of blank lines. down * blankLineHeight is used as the distance to move down for a blank
     * line.  */
    var blankLineScale = 1f
    var scaleX = 1f
    var scaleY = 1f
    var italic = false
    var bold = false
    var markupEnabled: Boolean = false
    /** The amount to add to the glyph X position when drawing a cursor between glyphs. This field is not set by the BMFont
     * file, it needs to be set manually depending on how the glyphs are rendered on the backing textures.  */
    var cursorX: Float = 0.toFloat()

    val glyphs = arrayOfNulls<Array<FNTGlyph2?>>(
        PAGES
    )
    /** The glyph to display for characters not in the font. May be null.  */
    var missingGlyph: FNTGlyph2? = null

    /** The x-height, which is the distance from the top of most lowercase characters to the baseline.  */
    var xHeight = 1f // ?????

    /** Additional characters besides whitespace where text is wrapped. Eg, a hypen (-).  */
    var breakChars: CharArray? = null
    var xChars = charArrayOf('x', 'e', 'a', 'o', 'n', 's', 'r', 'c', 'u', 'm', 'v', 'w', 'z')
    var capChars = charArrayOf('M', 'N', 'B', 'D', 'C', 'E', 'F', 'K', 'A', 'G', 'H', 'I', 'J', 'L', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z')

    val firstGlyph: FNTGlyph2
        get() {
            this.glyphs
                .filterNotNull()
                .forEach {
                    for (glyph in it) {
                        if (glyph?.height == 0 || glyph?.width == 0) continue
                        return glyph!!
                    }
                }
            throw Exception("No glyphs found.")
        }

    /** Creates an empty BitmapFontData for configuration before calling [.parse], to subclass, or to
     * populate yourself, e.g. using stb-truetype or FreeType.  */

    constructor(fntData: String, flip: Boolean) : this() {
        parse(fntData, flip)
    }

    private fun parseMetadata(line: String): Map<String, String> =
        line.split(" ".toRegex())
            .asSequence()
            .filter { it.isNotEmpty() }
            .map { it.split("=") }
            .associateBy({ if (it.size == 1) "_type" else it[0] }, {
                val value = if (it.size == 1) it[0] else it[1]
                if (value.startsWith("\"")) value.substring(1, value.length - 1) else value
            })

    private fun parseMetadataGroup(type: String, lines: Iterator<String>): List<Map<String, String>> {
        var find = false
        val result = ArrayList<Map<String, String>>()
        while (lines.hasNext()) {
            val line = lines.next()
            if (find && !line.startsWith(type)) break
            else if (!line.startsWith(type)) continue

            result.add(parseMetadata(line))
            find = true
        }
        return result
    }

    fun parse(fntData: String, flip: Boolean) {
        val lines = fntData.split("[\r\n]+".toRegex()).iterator()

        val infos = parseMetadata(lines.next())

        size = infos["size"]?.toFloat()?.absoluteValue ?: throw Exception("Invalid size.")
        bold = infos["bold"]?.toInt() == 1
        italic = infos["italic"]?.toInt() == 1
        face = infos["face"] ?: "unknown"
        val padding = infos["padding"]?.split(",".toRegex(), 4) ?: throw Exception("Invalid padding.")
        if (padding.size != 4) throw Exception("Invalid padding.")
        padTop = padding[0].toFloat()
        padRight = padding[1].toFloat()
        padBottom = padding[2].toFloat()
        padLeft = padding[3].toFloat()
        val padY = padTop + padBottom

        val common = parseMetadata(lines.next())
        common["lineHeight"]


        lineHeight = common["lineHeight"]?.toFloat() ?: throw Exception("Missing: lineHeight")
        val baseLine = common["base"]?.toFloat() ?: throw Exception("Missing: base")

        var pageCount = 1
        if ("pages" in common)
            pageCount = max(1, common["pages"]!!.toInt())


        pages = parseMetadataGroup("page", lines)
            .mapIndexed { index, it -> it["file"] ?: throw Exception("Missing: file for page $index") }
            .toTypedArray()
        if (pages.size != pageCount) throw Exception("Missing additional page definitions.")

        val chars = parseMetadataGroup("char ", lines)
        for (char in chars) {
            val glyph = FNTGlyph2()
            glyph.id = char["id"]?.toInt()!!
            if (glyph.id <= 0) missingGlyph = glyph
            if (glyph.id >= MAX_CHAR.toInt()) continue

            glyph.srcX = char["x"]?.toInt()!!
            glyph.srcY = char["y"]?.toInt()!!
            glyph.width = char["width"]?.toInt()!!
            glyph.height = char["height"]?.toInt()!!
            glyph.xoffset = char["xoffset"]?.toInt()!!
            glyph.yoffset = if (flip) char["yoffset"]?.toInt()!! else -(glyph.height + char["yoffset"]?.toInt()!!)
            glyph.xadvance = char["xadvance"]?.toInt()!!

            if ("page" in char)
                glyph.page = char["page"]?.toInt()!!

            if (glyph.width > 0 && glyph.height > 0) descent = min(baseLine + glyph.yoffset, descent)
            setGlyph(glyph.id, glyph)
        }
        descent += padBottom

        val kernings = parseMetadataGroup("kerning", lines)
        for (k in kernings) {
            //invert order
            val first = k["second"]!!.toInt()
            val second = k["first"]!!.toInt()

            if (first < 0 || first > MAX_CHAR.toInt() || second < 0 || second > MAX_CHAR.toInt()) continue
            val glyph = getGlyph(first.toChar())
            val amount = k["amount"]!!.toInt()

            glyph?.setKerning(second, amount)
        }

        // --- spaceWidth
        val spaceGlyph = getGlyph(' ') ?: getGlyph('1') ?: firstGlyph
        if (spaceGlyph.width == 0) {
            spaceGlyph.width = (padLeft + spaceGlyph.xadvance.toFloat() + padRight).toInt()
            spaceGlyph.xoffset = (-padLeft).toInt()
        }
        spaceWidth = spaceGlyph.width.toFloat()

        // --- xHeight ???
        var xGlyph: FNTGlyph2? = null
        for (xChar in xChars) {
            xGlyph = getGlyph(xChar)
            if (xGlyph != null) break
        }
        if (xGlyph == null) xGlyph = firstGlyph
        xHeight = xGlyph.height - padY

        // --- capHeight
        var capGlyph: FNTGlyph2? = null
        for (capChar in capChars) {
            capGlyph = getGlyph(capChar)
            if (capGlyph != null) break
        }
        if (capGlyph == null) {
            glyphs
                .asSequence()
                .filterNotNull()
                .forEach {
                    it
                        .asSequence()
                        .filter { it?.height != 0 && it?.width != 0 }
                        .forEach { capHeight = max(capHeight, it!!.height.toFloat()) }
                }
        } else
            capHeight = capGlyph.height.toFloat()
        capHeight -= padY

        // ---
        ascent = baseLine - capHeight
        down = -lineHeight
        if (flip) {
            ascent = -ascent
            down = -down
        }
    }

    fun setGlyph(ch: Int, glyph: FNTGlyph2) {
        var page: Array<FNTGlyph2?>? = glyphs[ch / PAGE_SIZE]
        if (page == null) {
            page = arrayOfNulls(PAGE_SIZE)
            glyphs[ch / PAGE_SIZE] = page
        }
        page[ch and PAGE_SIZE - 1] = glyph
    }

    /** Returns true if the font has the glyph, or if the font has a [.missingGlyph].  */
    fun hasGlyph(ch: Char): Boolean {
        return if (missingGlyph != null) true else getGlyph(ch) != null
    }

    /** Returns the glyph for the specified character, or null if no such glyph exists. Note that
     * [.getGlyphs] should be be used to shape a string of characters into a
     * list of glyphs.  */
    fun getGlyph(ch: Char): FNTGlyph2? {
        val page = glyphs[ch.toInt() / PAGE_SIZE]
        return if (page != null) page[ch.toInt() and PAGE_SIZE - 1] else null
    }

    /** Returns the first valid glyph index to use to wrap to the next line, starting get the specified start index and
     * (typically) moving toward the beginning of the glyphs array.  */
    fun getWrapIndex(glyphs: Array<FNTGlyph2>, start: Int): Int {
        var i = start - 1
        while (i >= 1) {
            if (!isWhitespace(glyphs[i].id as Char)) break
            i--
        }
        while (i >= 1) {
            val ch = glyphs[i].id as Char
            if (isWhitespace(ch) || isBreakChar(ch)) return i + 1
            i--
        }
        return 0
    }

    fun isBreakChar(c: Char): Boolean = breakChars?.contains(c) ?: false

    fun isWhitespace(c: Char): Boolean = when (c) {
        '\n', '\r', '\t', ' ' -> true
        else -> false
    }

}