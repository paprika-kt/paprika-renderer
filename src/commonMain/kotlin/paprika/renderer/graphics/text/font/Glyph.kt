package paprika.renderer.graphics.text.font

import paprika.renderer.graphics.sprite.Quad

interface Glyph {
    var character: Char
    val sprite: Quad
    val xOffset: Float
    fun getKerning(char: Char): Float
}