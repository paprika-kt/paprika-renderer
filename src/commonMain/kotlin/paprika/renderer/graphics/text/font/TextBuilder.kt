package paprika.renderer.graphics.text.font

import paprika.renderer.graphics.sprite.MultiSpriteBuilder
import paprika.renderer.PGLRenderingContext
import paprika.renderer.shader.Shader
import paprika.renderer.shader.SpriteShaderFactory
import paprika.renderer.toFloatArrayOf

class TextBuilder(val pgl: PGLRenderingContext) {
        val shader: Shader = SpriteShaderFactory.sprite(pgl)

        private val multiSprite = MultiSpriteBuilder()
        private var x = 0f
        private var y = 0f

        fun write(text: String, font: Font, fontStyle: FontStyle? = null) {
            var prev: Char? = null

            val scale = if (fontStyle?.size != null) fontStyle.size / font.size else 1f
            val color = fontStyle?.color?.toFloatArrayOf(4)
            val lineHeight = fontStyle?.lineHeight ?: font.size * scale + 2

            val vertices = FloatArray(8)
            for (it in text) {
                if (it == ' ') {
                    x += font.spaceWidth * scale
                    continue
                } else if (it == '\t') {
                    x += font.spaceWidth * 4 * scale
                    continue
                } else if (it == '\n') {
                    y += lineHeight
                    x = 0f
                    continue
                }

                val glyph = font[it] ?: TODO("character $it not found")
                val sprite = glyph.sprite
                vertices[0] = sprite.vertices[0] * scale + x
                vertices[1] = sprite.vertices[1] * scale + y
                vertices[2] = sprite.vertices[2] * scale + x
                vertices[3] = sprite.vertices[3] * scale + y
                vertices[4] = sprite.vertices[4] * scale + x
                vertices[5] = sprite.vertices[5] * scale + y
                vertices[6] = sprite.vertices[6] * scale + x
                vertices[7] = sprite.vertices[7] * scale + y

                multiSprite.add(sprite.texture, vertices, sprite.textCoord, sprite.indices, color ?: sprite.colors)

                val kerning = if (prev != null) glyph.getKerning(prev) else 0f
                x += (sprite.width + glyph.xOffset - kerning) * scale
                prev = it
            }
        }

        fun build() = multiSprite.build()
    }