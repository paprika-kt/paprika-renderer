package paprika.renderer.graphics.text.font

interface Font {
    val lineHeight: Float
    val size: Float
    val italic: Boolean
    val weight: Int
    val spaceWidth: Float
    /** The distance from the cap height to the top of the tallest glyph.  */
    val ascent: Float
    val glyphs: Map<Char, Glyph>
    operator fun get(char: Char): Glyph?
}