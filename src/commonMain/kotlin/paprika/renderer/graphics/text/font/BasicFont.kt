package paprika.renderer.graphics.text.font

class BasicFont(
    override var lineHeight: Float = 0f,
    override var size: Float = 0f,
    override val italic: Boolean,
    override val weight: Int,
    override var spaceWidth: Float = 0f,
    override var ascent: Float = 0f,
    override var glyphs: Map<Char, Glyph> = HashMap()
) : Font {
    override fun get(char: Char): Glyph? = glyphs[char]
}