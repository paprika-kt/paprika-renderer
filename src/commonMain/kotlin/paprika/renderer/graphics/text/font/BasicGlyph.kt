package paprika.renderer.graphics.text.font

import paprika.renderer.graphics.sprite.Quad

class BasicGlyph(override var character: Char, override val sprite: Quad, override val xOffset: Float = 0f, private val kerning: Map<Char, Float>) :
    Glyph {
    override fun getKerning(char: Char): Float = kerning[char] ?: 0f
}