package paprika.renderer.graphics.text.font

import paprika.renderer.graphics.Color

class FontStyle(
    val color: Color? = null,
    val size: Float? = null,
    val lineHeight: Float? = null
)