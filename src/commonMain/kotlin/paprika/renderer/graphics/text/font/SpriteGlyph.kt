package paprika.renderer.graphics.text.font

import paprika.renderer.graphics.sprite.Quad

class SpriteGlyph(override var character: Char, override val sprite: Quad) :
    Glyph {
    override val xOffset: Float = 0f
    override fun getKerning(char: Char): Float = 0f
}