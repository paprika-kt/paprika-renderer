package paprika.renderer.graphics.text.font

import paprika.renderer.graphics.Color
import paprika.renderer.graphics.sprite.Quad
import paprika.renderer.graphics.sprite.color

class TransformedGlyph(private val resize: Float, private val color: Color?, private val glyph: Glyph) :
    Glyph {
    override var character: Char = glyph.character
    override val sprite: Quad = glyph.sprite.clone().apply {
        set(x * resize, y * resize, width * resize, height * resize)
        if (this@TransformedGlyph.color != null)
            color = this@TransformedGlyph.color
    }
    override val xOffset: Float = glyph.xOffset * resize
    override fun getKerning(char: Char): Float = glyph.getKerning(char) * resize
}