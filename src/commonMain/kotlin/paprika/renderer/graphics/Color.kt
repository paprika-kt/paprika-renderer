@file:Suppress("unused")

package paprika.renderer.graphics

class MutableColor(
    override var red: Float,
    override var green: Float,
    override var blue: Float,
    override var alpha: Float = 1f
) : Color {

    fun set(color: Color) {
        red = color.red
        green = color.green
        blue = color.blue
        alpha = color.alpha
    }

    fun set(red: Float, green: Float, blue: Float, alpha: Float = 1f) {
        this.red = red
        this.green = green
        this.blue = blue
        this.alpha = alpha
    }

    fun clone(): Color {
        return MutableColor(red, green, blue, alpha)
    }

    companion object {
        fun fromRGBA(red: Int, green: Int, blue: Int, alpha: Int = 255) =
            MutableColor(red / 255f, green / 255f, blue / 255f, alpha / 255f)

        fun fromRGB(rgb: Int) = fromRGBA((rgb shr 16) and 0xFF, (rgb shr 8) and 0xFF, (rgb shr 0) and 0xFF, 1)
        fun fromRGBA(argb: Int) =
            fromRGBA((argb shr 24) and 0xFF, (argb shr 16) and 0xFF, (argb shr 8) and 0xFF, (argb shr 0) and 0xFF)

        fun fromFloatArray(color: FloatArray): Color = MutableColor(color[0], color[1], color[2], color[3])
    }
}

data class ReadOnlyColor(
    override val red: Float,
    override val green: Float,
    override val blue: Float,
    override val alpha: Float = 1f
) : Color

interface Color {
    val red: Float
    val green: Float
    val blue: Float
    val alpha: Float

    operator fun component1() = red
    operator fun component2() = green
    operator fun component3() = blue
    operator fun component4() = alpha

    val redChanel
        get() = (red * 255).toInt()
    val greenChanel
        get() = (green * 255).toInt()
    val blueChanel
        get() = (blue * 255).toInt()
    val alphaChanel
        get() = (alpha * 255).toInt()

    fun toRGBInt() = (((redChanel shl 8) + greenChanel) shl 8) + blueChanel
    fun toRGBAInt() = (((((redChanel shl 8) + greenChanel) shl 8) + blueChanel) shl 8) + alphaChanel

    companion object {
        fun create(red: Float, green: Float, blue: Float, alpha: Float = 1f) = ReadOnlyColor(red, green, blue, alpha)
        fun fromRGBA(red: Int, green: Int, blue: Int, alpha: Int = 255) =
            ReadOnlyColor(red / 255f, green / 255f, blue / 255f, alpha / 255f)

        fun fromRGB(rgb: Int) =
            if (rgb >= 0 && rgb <= 0xFFFFFF) fromRGBA(
                (rgb shr 16) and 0xFF,
                (rgb shr 8) and 0xFF,
                (rgb shr 0) and 0xFF,
                255
            ) else NOT_SUPPORTED_VALUE()

        fun fromRGBA(rgba: Long) = if (rgba >= 0 && rgba <= 0xFFFFFFFF) fromRGBA(
            ((rgba shr 24) and 0xFF).toInt(),
            ((rgba shr 16) and 0xFF).toInt(), ((rgba shr 8) and 0xFF).toInt(), ((rgba shr 0) and 0xFF).toInt()
        ) else NOT_SUPPORTED_VALUE()

        fun fromFloatArray(color: FloatArray): Color = ReadOnlyColor(color[0], color[1], color[2], color[3])

        val WHITE = ReadOnlyColor(1f, 1f, 1f)
        val BLACK = ReadOnlyColor(0f, 0f, 0f)
        val RED: Color = ReadOnlyColor(1f, 0f, 0f)
        val GREEN: Color = ReadOnlyColor(0f, 1f, 0f)
        val BLUE: Color = ReadOnlyColor(0f, 0f, 1f)
        val GRAY: Color = ReadOnlyColor(0.5f, 0.5f, 0.5f)
    }
}

class UnsuportedValueException : Throwable()

fun NOT_SUPPORTED_VALUE(): Nothing = throw UnsuportedValueException()

fun Color.toMutable(): MutableColor = MutableColor(red, green, blue, alpha)

fun colorOf(red: Int, green: Int, blue: Int, alpha: Int = 255): Color = Color.fromRGBA(red, green, blue, alpha)
fun colorOf(color: Int, alpha: Boolean = true): Color =
    if (alpha) Color.fromRGBA(color.toLong()) else Color.fromRGB(color)

fun colorOf(color: Long, alpha: Boolean = true): Color =
    if (alpha) Color.fromRGBA(color) else Color.fromRGB(color.toInt())

fun colorOf(color: FloatArray): Color = Color.fromFloatArray(color)