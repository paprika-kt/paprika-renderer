@file:Suppress("unused")

package paprika.renderer.graphics.texture

import ca.jsbr.disposable.Disposable
import paprika.canvas.sources.ImageSource
import paprika.render.RenderingContext
import paprika.render.pglObject.PGLTexture
import paprika.renderer.graphics.Color

object TextureFilter {
    val LINEAR = RenderingContext.LINEAR
    val NEAREST = RenderingContext.NEAREST
}

fun ImageSource.toTexture(gl: RenderingContext): Texture = MutableTexture(gl)
        .also { it.setSource(this) }

/**
 * Review use static for create from source, from bytes, from TextureData
 * Review (implement PrimitiveTexture)
 */
class MutableTexture(val gl: RenderingContext) : Texture, Disposable {
    override var width: Int = 0
    override var height: Int = 0
    override val id: PGLTexture = gl.createTexture()

    fun setFilter(minFilter: Int, magFilter: Int) {
        gl.texParameteri(RenderingContext.TEXTURE_2D, RenderingContext.TEXTURE_MIN_FILTER, minFilter)
        gl.texParameteri(RenderingContext.TEXTURE_2D, RenderingContext.TEXTURE_MAG_FILTER, magFilter)
    }


    fun setColor(color: Color, width: Int, height: Int) {
        this.width = width
        this.height = height

        setColor(gl, color)
    }

    fun setSource(source: ImageSource, mipmap: Boolean = true) {
        width = source.width
        height = source.height
        setSource(gl, source, mipmap)
    }

    override fun dispose() {
        gl.deleteTexture(id)
    }
}

