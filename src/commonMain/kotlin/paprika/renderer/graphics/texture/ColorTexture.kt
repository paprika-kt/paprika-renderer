package paprika.renderer.graphics.texture

import paprika.io.buffer.ByteArrayBuffer
import paprika.io.buffer.set
import paprika.renderer.graphics.Color
import paprika.renderer.PGL
import paprika.render.pglObject.*


class ColorTexture(val width: Int, val height: Int, val color: Color) {

    private var _id: PGLTexture? = null
     val id: PGLTexture
        get() = _id ?: error("not created")


    fun getOrCreate(gl: PGL): PGLTexture {
        _id = gl.createTexture()

        val bytes = ByteArrayBuffer(width * height * 4)
        for (it in 0 until bytes.size step 4) {
            bytes[it] = color.redChanel.toUShort()
            bytes[it + 1] = color.greenChanel.toUShort()
            bytes[it + 2] = color.blueChanel.toUShort()
            bytes[it + 3] = color.alphaChanel.toUShort()
        }
        bytes.position = 0


        val bytes2 = ByteArrayBuffer(width * height * 4)
        for (it in 0 until bytes.size step 4) {
            bytes2[it] = color.redChanel.toByte()
            bytes2[it + 1] = color.greenChanel.toByte()
            bytes2[it + 2] = color.blueChanel.toByte()
            bytes2[it + 3] = color.alphaChanel.toByte()
        }
        bytes2.position = 0

        gl.bindTexture(PGL.TEXTURE_2D, id)
        gl.texImage2D(PGL.TEXTURE_2D, 0, PGL.RGBA, width, height, 0, PGL.RGBA, PGL.UNSIGNED_BYTE, bytes)

        val isPowerOf2 = (width and (width - 1)) == 0 && (height and (height - 1)) == 0

        if (isPowerOf2) {
            // Oui, c'est une puissance de 2. Générer les mips.
            gl.generateMipmap(PGL.TEXTURE_2D)
        } else {
            // Non, ce n'est pas une puissance de 2. Désactiver les mips et définir l'habillage
            // comme "accrocher au bord"
            gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_WRAP_S, PGL.CLAMP_TO_EDGE)
            gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_WRAP_T, PGL.CLAMP_TO_EDGE)
        }
        gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_MIN_FILTER, PGL.LINEAR)
        gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_MAG_FILTER, PGL.LINEAR)
        return _id!!
    }
}