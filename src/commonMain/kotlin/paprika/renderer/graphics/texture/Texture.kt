package paprika.renderer.graphics.texture

import paprika.render.pglObject.PGLTexture


interface Texture {
    val width: Int
    val height: Int
    val id: PGLTexture
}