package paprika.renderer.graphics.texture

import paprika.renderer.graphics.texture.Texture
import paprika.renderer.graphics.Color
import paprika.canvas.sources.ImageSource
import paprika.io.buffer.ByteArrayBuffer
import paprika.renderer.PGL
import paprika.render.pglObject.*
import paprika.render.pglObject.*


fun Texture.setColor(gl: PGL, color: Color) {
    val bytes = ByteArrayBuffer(width * height * 4)
    for (it in 0 until bytes.size step 4) {
        bytes[it] = color.redChanel.toByte()
        bytes[it + 1] = color.greenChanel.toByte()
        bytes[it + 2] = color.blueChanel.toByte()
        bytes[it + 3] = color.alphaChanel.toByte()
    }
    bytes.position = 0


    val bytes2 = ByteArrayBuffer(width * height * 4)
    for (it in 0 until bytes.size step 4) {
        bytes2[it] = color.redChanel.toByte()
        bytes2[it + 1] = color.greenChanel.toByte()
        bytes2[it + 2] = color.blueChanel.toByte()
        bytes2[it + 3] = color.alphaChanel.toByte()
    }
    bytes2.position = 0

    gl.bindTexture(PGL.TEXTURE_2D, id)
    gl.texImage2D(PGL.TEXTURE_2D, 0, PGL.RGBA, width, height, 0, PGL.RGBA, PGL.UNSIGNED_BYTE, bytes)

    val isPowerOf2 = (width and (width - 1)) == 0 && (height and (height - 1)) == 0

    if (isPowerOf2) {
        // Oui, c'est une puissance de 2. Générer les mips.
        gl.generateMipmap(PGL.TEXTURE_2D)
    } else {
        // Non, ce n'est pas une puissance de 2. Désactiver les mips et définir l'habillage
        // comme "accrocher au bord"
        gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_WRAP_S, PGL.CLAMP_TO_EDGE)
        gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_WRAP_T, PGL.CLAMP_TO_EDGE)
    }
    gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_MIN_FILTER, PGL.LINEAR)
    gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_MAG_FILTER, PGL.LINEAR)
}

fun Texture.setSource(gl: PGL, source: ImageSource, mipmap: Boolean = true) {
    val isPowerOf2 = (width and (width - 1)) == 0 && (height and (height - 1)) == 0
    gl.bindTexture(PGL.TEXTURE_2D, id)

    gl.texImage2D(
        PGL.TEXTURE_2D, 0, PGL.RGBA,
        PGL.RGBA, PGL.UNSIGNED_BYTE, source
    )

    // WebGL1 a des spécifications différentes pour les images puissances de 2
    // par rapport aux images non puissances de 2 ; aussi vérifier si l'image est une
    // puissance de 2 sur chacune de ses dimensions.
    if (isPowerOf2) {
        // Oui, c'est une puissance de 2. Générer les mips.
        gl.generateMipmap(PGL.TEXTURE_2D)
    } else {
        // Non, ce n'est pas une puissance de 2. Désactiver les mips et définir l'habillage
        // comme "accrocher au bord"
        gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_WRAP_S, PGL.CLAMP_TO_EDGE)
        gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_WRAP_T, PGL.CLAMP_TO_EDGE)
    }
    gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_MIN_FILTER, PGL.LINEAR)
    gl.texParameteri(PGL.TEXTURE_2D, PGL.TEXTURE_MAG_FILTER, PGL.LINEAR)


}