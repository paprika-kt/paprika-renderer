package paprika.renderer

import ca.jsbr.paprika.engine.material.DefaultSpriteMaterial
import ca.jsbr.paprika.engine.material.SpriteMaterial
import paprika.koml.geom.MutableMatrix4f
import paprika.io.buffer.FloatArrayBuffer
import paprika.io.buffer.ShortArrayBuffer
import paprika.render.RenderingContext
import paprika.render.pglObject.PGLBuffer
import paprika.renderer.graphics.texture.Texture
import paprika.renderer.material.TestSpriteMaterial
import paprika.renderer.shader.SpriteShaderFactory


class OrderedBatchingRenderer2D(val gl: PGLRenderingContext, override var perspective: MutableMatrix4f, private val batchSize: Int = 100) : Renderer2D, Renderer {

    // merge floatArray?
    private val vertices = FloatArrayBuffer(batchSize * 2) // transform to array
    private val texCoord = FloatArrayBuffer(batchSize * 2)
    private val indices = ShortArrayBuffer(batchSize * 3)
    private val colors = FloatArrayBuffer(batchSize * 4)

    private var verticesCount = 0
    private var indicesCount = 0
    private var drawCount = 0

    private val indexBuffer = gl.createBuffer()
    private val texCoordBuffer = gl.createBuffer()
    private val positionBuffer = gl.createBuffer()
    private val colorBuffer = gl.createBuffer()

    override var spriteMaterial: SpriteMaterial = DefaultSpriteMaterial(SpriteShaderFactory.spriteColor(gl))
//            override var spriteMaterial: SpriteMaterial = TestSpriteMaterial(SpriteShaderFactory.white(gl))
        set(value) {
            if (value != field)
                flush()
            field = value
        }

    //TODO review only need PGLTexture
    private var lastTexture: Texture? = null
        set(value) {
            if (field != null && value != field)
                flush()
            field = value
        }

    override fun draw(texture: Texture, x: Float, y: Float, width: Float, height: Float, u: Float, v: Float, u2: Float, v2: Float, r: Float, g: Float, b: Float, a: Float) {

        if (batchSize < verticesCount + 4)
            flush()

        lastTexture = texture

        var startVertices = verticesCount * 2
        var startTextCoord = verticesCount * 2
        var startColor = verticesCount * 4

        this.vertices[startVertices++] = x
        this.vertices[startVertices++] = y

        this.vertices[startVertices++] = x
        this.vertices[startVertices++] = height + y

        this.vertices[startVertices++] = width + x
        this.vertices[startVertices++] = height + y

        this.vertices[startVertices++] = width + x
        this.vertices[startVertices] = y

        this.texCoord[startTextCoord++] = u
        this.texCoord[startTextCoord++] = v
        this.texCoord[startTextCoord++] = u
        this.texCoord[startTextCoord++] = v2
        this.texCoord[startTextCoord++] = u2
        this.texCoord[startTextCoord++] = v2
        this.texCoord[startTextCoord++] = u2
        this.texCoord[startTextCoord] = v

        this.colors[startColor++] = r
        this.colors[startColor++] = g
        this.colors[startColor++] = b
        this.colors[startColor++] = a
        this.colors[startColor++] = r
        this.colors[startColor++] = g
        this.colors[startColor++] = b
        this.colors[startColor++] = a
        this.colors[startColor++] = r
        this.colors[startColor++] = g
        this.colors[startColor++] = b
        this.colors[startColor++] = a
        this.colors[startColor++] = r
        this.colors[startColor++] = g
        this.colors[startColor++] = b
        this.colors[startColor] = a

        this.indices[indicesCount++] = (3 + verticesCount).toShort()
        this.indices[indicesCount++] = (2 + verticesCount).toShort()
        this.indices[indicesCount++] = (1 + verticesCount).toShort()
        this.indices[indicesCount++] = (3 + verticesCount).toShort()
        this.indices[indicesCount++] = (1 + verticesCount).toShort()
        this.indices[indicesCount++] = (0 + verticesCount).toShort()

        verticesCount += 4
    }

    override fun draw(texture: Texture, vertices: FloatArray, textCoord: FloatArray, indices: ShortArray, colors: FloatArray) {
        val len = vertices.size / 2
        if (batchSize < verticesCount + len * 2)
            flush()

        lastTexture = texture
        var count = 0

        while (count < len) {

            var index = count * 2
            var start = verticesCount * 2
//vertices
            this.vertices[start + index] = vertices[index]
            this.vertices[start + index + 1] = vertices[index + 1]
//texture coordinate
            this.texCoord[start + index] = textCoord[index]
            this.texCoord[start + index + 1] = textCoord[index + 1]
//colors
            index = count * 4
            start = verticesCount * 4
            this.colors[start + index] = colors[index]
            this.colors[start + index + 1] = colors[index + 1]
            this.colors[start + index + 2] = colors[index + 2]
            this.colors[start + index + 3] = colors[index + 3]
            count++
        }

        for (it in indices) {
            this.indices[indicesCount++] = (it + verticesCount).toShort()
        }

        verticesCount += len
    }


    override fun flush() {
        drawCount++
        if (lastTexture == null || verticesCount == 0)
            return

        gl.disable(PGL.CULL_FACE)
//        gl.cullFace(PGL.FRONT_AND_BACK)

        gl.enable(PGL.BLEND)
        gl.disable(PGL.DEPTH_TEST)
        gl.blendFunc(PGL.SRC_ALPHA, PGL.ONE_MINUS_SRC_ALPHA)
        gl.bindTexture(PGL.TEXTURE_2D, lastTexture!!.id)
        gl.useProgram(spriteMaterial.shaderProgram)

        spriteMaterial.setCoordinates(applyPositionBuffer())
        spriteMaterial.setTextureCoordinates(applyTexcoordBuffer())
        spriteMaterial.setColor(applyColorBuffer())
        // IMPORTANT: Unbind from the buffer when we're done with it.
        gl.bindBuffer(PGL.ARRAY_BUFFER, null)

        spriteMaterial.setMatrix(perspective)

        applyIndexBuffer()
        gl.drawElements(PGL.TRIANGLES, indicesCount, PGL.UNSIGNED_SHORT, 0)

        indicesCount = 0
        verticesCount = 0
    }

    override fun end() {
        drawCount = 0
        flush()
//        vertices.reset()
//        indices.reset()
//        texCoord.reset()
//        colors.reset()
    }


    private fun applyIndexBuffer(): PGLBuffer {
        indices.position = 0
        gl.bindBuffer(PGL.ELEMENT_ARRAY_BUFFER, indexBuffer)
        gl.bufferData(PGL.ELEMENT_ARRAY_BUFFER, indices, PGL.STATIC_DRAW)
        return indexBuffer
    }

    private fun applyTexcoordBuffer(): PGLBuffer {
        texCoord.position = 0
        gl.bindBuffer(PGL.ARRAY_BUFFER, texCoordBuffer)
        gl.bufferData(PGL.ARRAY_BUFFER, texCoord, PGL.STATIC_DRAW)
        return texCoordBuffer
    }

    private fun applyPositionBuffer(): PGLBuffer {
        vertices.position = 0
        gl.bindBuffer(PGL.ARRAY_BUFFER, positionBuffer)
        gl.bufferData(PGL.ARRAY_BUFFER, vertices, PGL.STATIC_DRAW)
        return positionBuffer
    }

    private fun applyColorBuffer(): PGLBuffer {
        colors.position = 0
        gl.bindBuffer(PGL.ARRAY_BUFFER, colorBuffer)
        gl.bufferData(PGL.ARRAY_BUFFER, colors, PGL.STATIC_DRAW)
        return colorBuffer
    }
}
