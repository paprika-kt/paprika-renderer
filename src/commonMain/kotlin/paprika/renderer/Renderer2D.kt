package paprika.renderer

import ca.jsbr.paprika.engine.material.SpriteMaterial
import paprika.renderer.graphics.sprite.Sprite
import paprika.renderer.graphics.texture.Texture

interface Renderer2D : Renderer {
//    var spriteMaterial: SpriteMaterial // review  maybe pass in method
    @Deprecated("use Sprite", replaceWith = ReplaceWith("draw"))
    fun draw(texture: Texture) {
        texture.width.toFloat()
        texture.height.toFloat()
    }
    @Deprecated("use Sprite", replaceWith = ReplaceWith("draw"))
    fun draw(texture: Texture, x: Float, y: Float, width: Float, height: Float, u: Float = 0f, v: Float = 0f, u2: Float = 1f, v2: Float = 1f, r: Float = 1f, g: Float = 1f, b: Float = 1f, a: Float = 1f)

    fun draw(sprite: Sprite) = draw(sprite.texture, sprite.vertices, sprite.textCoord, sprite.indices, sprite.colors) //TODO review extension method

    fun draw(texture: Texture, vertices: FloatArray, textCoord: FloatArray, indices: ShortArray, colors: FloatArray)

    var spriteMaterial: SpriteMaterial
}

