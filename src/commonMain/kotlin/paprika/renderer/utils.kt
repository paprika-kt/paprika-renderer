package paprika.renderer

import paprika.render.RenderingContext

typealias PGL = RenderingContext
typealias PGLRenderingContext = RenderingContext