package paprika.renderer.shader

import ca.jsbr.paprika.engine.material.DefaultSpriteMaterial
import paprika.renderer.PGLRenderingContext
import paprika.renderer.shader.Shader

object FontShaderFactory {

    fun sdf(gl: PGLRenderingContext): Shader {
        val vs = """
        attribute vec2 ${DefaultSpriteMaterial.TEXCOORD_ATTRIBUTE};
        attribute vec3 ${DefaultSpriteMaterial.POSITION_ATTRIBUTE};
        attribute vec4 ${DefaultSpriteMaterial.COLOR_ATTRIBUTE};

        uniform mat4 ${DefaultSpriteMaterial.MATRIX_UNIFORM};

        varying vec2 v_texcoord;
        varying vec4 v_color;

        void main() {
            gl_Position = ${DefaultSpriteMaterial.MATRIX_UNIFORM} * vec4(${DefaultSpriteMaterial.POSITION_ATTRIBUTE}, 1.0);
                   v_texcoord = ${DefaultSpriteMaterial.TEXCOORD_ATTRIBUTE};
                   v_color = ${DefaultSpriteMaterial.COLOR_ATTRIBUTE};
        }
    """

        val fs = """
            #ifdef GL_OES_standard_derivatives
            #extension GL_OES_standard_derivatives : enable
            #endif
             #if __VERSION__ == 100
            precision mediump float;
            #endif

            uniform sampler2D ${DefaultSpriteMaterial.TEXTURE};

            varying vec2 v_texcoord;
            varying vec4 v_color;

            float aastep(float value) {
              #ifdef GL_OES_standard_derivatives
                float afwidth = length(vec2(dFdx(value), dFdy(value))) * 0.70710678118654757;
              #else
                float afwidth = (6.0 / 32.0) * (1.4142135623730951 / (2.0 * gl_FragCoord.w));
              #endif
              return smoothstep(0.5 - afwidth, 0.5 + afwidth, value);
            }
            void main() {
              vec4 texColor = texture2D(${DefaultSpriteMaterial.TEXTURE}, v_texcoord);
              float alpha = aastep(texColor.a);
              gl_FragColor = vec4(v_color.rgb, v_color.a * alpha);;
              if (gl_FragColor.a < 0.0001) discard;
            }
            """
        return Shader(gl, vs, fs)
    }

    fun sdf2(gl: PGLRenderingContext): Shader {
        val vs = """

        attribute vec2 ${DefaultSpriteMaterial.TEXCOORD_ATTRIBUTE};
        attribute vec3 ${DefaultSpriteMaterial.POSITION_ATTRIBUTE};
        attribute vec4 ${DefaultSpriteMaterial.COLOR_ATTRIBUTE};

        uniform mat4 ${DefaultSpriteMaterial.MATRIX_UNIFORM};

        varying vec2 v_texcoord;
        varying vec4 v_color;

        void main() {
            gl_Position = ${DefaultSpriteMaterial.MATRIX_UNIFORM} * vec4(${DefaultSpriteMaterial.POSITION_ATTRIBUTE}, 1.0);
                   v_texcoord = ${DefaultSpriteMaterial.TEXCOORD_ATTRIBUTE};
                   v_color = ${DefaultSpriteMaterial.COLOR_ATTRIBUTE};
        }
    """

        val fs = """

             #if __VERSION__ == 100
            precision mediump float;
            #endif

            uniform sampler2D ${DefaultSpriteMaterial.TEXTURE};

            varying vec2 v_texcoord;
            varying vec4 v_color;

            const float smoothing = 3.0/16.0;

            void main() {
                float distance = texture2D(${DefaultSpriteMaterial.TEXTURE}, v_texcoord).a;
                float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);
                gl_FragColor = vec4(v_color.rgb, v_color.a * alpha);
            }
            """
        return Shader(gl, vs, fs)
    }


    /**
     * https://github.com/Chlumsky/msdfgen#using-a-multi-channel-distance-field
     */
    fun msdf(gl: PGLRenderingContext): Shader {

        val vs = """

        attribute vec2 ${DefaultSpriteMaterial.TEXCOORD_ATTRIBUTE};
        attribute vec3 ${DefaultSpriteMaterial.POSITION_ATTRIBUTE};
        attribute vec4 ${DefaultSpriteMaterial.COLOR_ATTRIBUTE};

        uniform mat4 ${DefaultSpriteMaterial.MATRIX_UNIFORM};

        varying vec2 v_texcoord;
        varying vec4 v_color;

        void main() {
            gl_Position = ${DefaultSpriteMaterial.MATRIX_UNIFORM} * vec4(${DefaultSpriteMaterial.POSITION_ATTRIBUTE}, 1.0);
                   v_texcoord = ${DefaultSpriteMaterial.TEXCOORD_ATTRIBUTE};
                   v_color = ${DefaultSpriteMaterial.COLOR_ATTRIBUTE};
        }
    """

        val fs = """
            #ifdef GL_OES_standard_derivatives
            #extension GL_OES_standard_derivatives : enable
            #endif
            #if __VERSION__ == 100
            precision mediump float;
            #endif

            uniform sampler2D ${DefaultSpriteMaterial.TEXTURE};

            varying vec2 v_texcoord;
            varying vec4 v_color;

            float median(float r, float g, float b) {
                return max(min(r, g), min(max(r, g), b));
            }
            void main() {
                vec3 sample = 1.0 - texture2D(${DefaultSpriteMaterial.TEXTURE}, vUv).rgb;
                float sigDist = median(sample.r, sample.g, sample.b) - 0.5;
                float alpha = clamp(sigDist/fwidth(sigDist) + 0.5, 0.0, 1.0);
                gl_FragColor = v_color;
                if (gl_FragColor.a < 0.0001) discard;
            }
            """
        return Shader(gl, vs, fs)
    }
}