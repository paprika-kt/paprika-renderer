package paprika.renderer.shader

import paprika.renderer.graphics.texture.Texture
import paprika.koml.geom.MutableMatrix4f
import paprika.render.pglObject.PGLProgram
import paprika.renderer.PGL
import paprika.renderer.PGLRenderingContext


class ShaderUniformProperty(val gl: PGLRenderingContext, program: PGLProgram, val name: String, val type: String) {

    val location = gl.getUniformLocation(program, name)


    fun setColor(color: Array<Float>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun setMatrix4f(matrix4f: MutableMatrix4f) = setMatrix4f(matrix4f.values)

    fun setMatrix4f(matrix4f: FloatArray) {
        gl.uniformMatrix4fv(location, false, matrix4f)
    }

    fun setTexture(texture: Texture) {
        gl.activeTexture(PGL.TEXTURE0)
        gl.bindTexture(PGL.TEXTURE_2D, texture.id)
        gl.uniform1i(location, 0)
    }
}