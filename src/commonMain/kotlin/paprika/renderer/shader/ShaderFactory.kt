package paprika.renderer.shader

import ca.jsbr.paprika.engine.material.DefaultSpriteMaterial
import paprika.renderer.PGLRenderingContext

object ShaderFactory {


    fun spriteColorSwipe(gl: PGLRenderingContext): Shader {


        val vs = """
        attribute vec3 ${DefaultSpriteMaterial.POSITION_ATTRIBUTE};
        attribute vec2 ${DefaultSpriteMaterial.TEXCOORD_ATTRIBUTE};
        attribute vec4 ${DefaultSpriteMaterial.COLOR_ATTRIBUTE};

        uniform mat4 ${DefaultSpriteMaterial.MATRIX_UNIFORM};

        varying vec2 v_texcoord;
        varying vec4 v_color;

        void main() {
           gl_Position = ${DefaultSpriteMaterial.MATRIX_UNIFORM} * vec4(${DefaultSpriteMaterial.POSITION_ATTRIBUTE}, 1.0);
           v_texcoord = ${DefaultSpriteMaterial.TEXCOORD_ATTRIBUTE};
           v_color = ${DefaultSpriteMaterial.COLOR_ATTRIBUTE};
        }
    """.trimIndent()

        val fs = """
        #ifdef GL_ES
        precision mediump float;
        #endif
        varying vec2 v_texcoord;
        varying vec4 v_color;

        uniform sampler2D ${DefaultSpriteMaterial.TEXTURE};

        void main() {
           gl_FragColor = v_color * texture2D(${DefaultSpriteMaterial.TEXTURE}, v_texcoord);
        }
    """.trimIndent()
        TODO()
//        return Shader(gl, vs, fs)
    }
}