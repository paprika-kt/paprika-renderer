package paprika.renderer.shader

import paprika.render.pglObject.PGLBuffer
import paprika.render.pglObject.PGLProgram
import paprika.render.pglObject.PGLShader
import paprika.renderer.PGL
import paprika.renderer.PGLRenderingContext

class Shader(val gl: PGLRenderingContext, private val vertex: String, private val fragment: String) {

    var _shaderProgram: PGLProgram? = null
    fun getOrCreate() = _shaderProgram ?: create()

    private val uniform = HashMap<String, ShaderUniformProperty>()
    private val attribute = HashMap<String, ShaderAttribProperty>()

    private var regExProperty = Regex("(attribute|uniform|varying)\\s*([a-zA-Z0-9]*)\\s([a-zA-Z0-9_]*)")


    fun getVertexProperty(name: String): ShaderAttribProperty {
        return attribute[name] ?: TODO()
    }

    fun getConstantProperty(name: String): ShaderUniformProperty {
        return uniform[name] ?: TODO(name)
    }

    fun setAttribute(property: String, buffer: PGLBuffer, size: Int, type: Int) {
        gl.bindBuffer(PGL.ARRAY_BUFFER, buffer)
        gl.enableVertexAttribArray(attribute[property]!!.location)
        // review use stride for merge FloatArray
        gl.vertexAttribPointer(attribute[property]!!.location, size, type, false, 0, 0)
    }

    fun setIndices(indexBuffer: PGLBuffer) {
        gl.bindBuffer(PGL.ELEMENT_ARRAY_BUFFER, indexBuffer)
    }

    // TODO should be a shader factory an return a shader
    fun create(): PGLProgram {
        val vertShader = gl.createShader(PGL.VERTEX_SHADER)
        gl.shaderSource(vertShader, vertex)
        gl.compileShader(vertShader)

        // Create fragment shader object
        val fragShader = gl.createShader(PGL.FRAGMENT_SHADER)
        gl.shaderSource(fragShader, fragment)
        gl.compileShader(fragShader)

        // Create a shader program object to store
        // the combined shader program
        val program = gl.createProgram() ?: throw Exception("Fail to create Shader Program")

        // Attach a vertex shader
        gl.attachShader(program, vertShader)

        // Attach a fragment shader
        gl.attachShader(program, fragShader)

        validateShader(vertShader, "vertex")
        validateShader(fragShader, "fragment")

        // Link both the programs
        gl.linkProgram(program)
         validateProgram(program)
        _shaderProgram = program

//        gl.useProgram(program)
        parse(vertex, program)
        parse(fragment, program)
//        gl.useProgram(null)
        return program
    }

    private fun validateShader(shader: PGLShader, name: String) {
        val compiled = gl.getShaderParameter(shader, PGL.COMPILE_STATUS)
        val shaderLog = gl.getShaderInfoLog(shader)
        if (shaderLog!!.trim { it <= ' ' }.isNotEmpty()) println("$name shaderLog: $shaderLog")
        //if empty check OpenGL context is started printf("OpenGL version is (%s)\n", glGetString(GL_VERSION));
        if (compiled == 0) throw AssertionError("Could not compile $name status: $compiled shader: $shaderLog ")
    }

    private fun validateProgram(program: PGLProgram) {
        val linked = gl.getProgramParameter(program, PGL.LINK_STATUS)
        val programLog = gl.getProgramInfoLog(program)
        if (programLog?.trim()?.isNotEmpty() == true)
            println("programLog :$programLog")
        if (linked == 0)
            throw AssertionError("Could not link program: $programLog")
    }

    private fun parse(shader: String, program: PGLProgram) {
        regExProperty.findAll(shader)
            .forEach {
                val scope = it.groupValues[1]
                val type = it.groupValues[2]
                val name = it.groupValues[3]

                if (scope == "uniform")
                    uniform[name] = ShaderUniformProperty(gl, program, name, type)
                else
                    attribute[name] = ShaderAttribProperty(gl, program, name, type)
            }
    }
}

