package paprika.renderer.shader

import paprika.render.pglObject.PGLBuffer
import paprika.render.pglObject.PGLProgram
import paprika.renderer.PGLRenderingContext

class ShaderAttribProperty(val gl: PGLRenderingContext, val program: PGLProgram, val name: String, val type: String) {

    val location: Int = gl.getAttribLocation(program, name)
    val buffer: PGLBuffer = gl.createBuffer()
}
