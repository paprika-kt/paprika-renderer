package paprika.renderer.proto

import paprika.io.buffer.FloatArrayBuffer
import paprika.render.pglObject.PGLTexture

interface Render2D {
    fun draw(
        texture: PGLTexture,
        vertices: FloatArrayBuffer,
        texCoord: FloatArrayBuffer,
        indices: ShortIterator,
        colors: FloatArrayBuffer
    )
}