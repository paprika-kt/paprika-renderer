package paprika.renderer

import paprika.koml.geom.MutableMatrix4f

interface Renderer {
    var perspective: MutableMatrix4f
    fun flush()
    fun end()
}