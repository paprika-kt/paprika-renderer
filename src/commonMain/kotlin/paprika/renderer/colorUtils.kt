
package paprika.renderer

import paprika.renderer.graphics.Color

fun Color.toFloatArrayOf(count: Int): FloatArray {
        return (0 until count).map { listOf(red, green, blue, alpha) }.flatten().toFloatArray()
    }

